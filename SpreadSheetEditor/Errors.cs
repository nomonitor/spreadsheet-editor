﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpreadSheetEditor
{
    public class Errors
    {
        public enum errors
        {
            no_error = 0,
            wrong_expressions = 1,
            selfloop = 2,
            infloop = 3,
            no_reference = 4,
            not_allowed = 5,
        };
        private static String[] error_desc = {
            null,
            "#not allowed",
            "#wrong expressions",
            "#selfloop",
            "#infinity loop",
            "#no reference"

        };
        public static String getErrorDesc(errors input)
        {
            return error_desc[(int)input];
        }
    }
}
