﻿using System;
using System.Data;
using System.Text;
using System.Windows;
using System.Windows.Input;
using System.Threading.Tasks;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Diagnostics;
using System.Data.OleDb;
using System.ComponentModel;

namespace SpreadSheetEditor
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private DataSet SpreadSheet { get; set; }

        private Dictionary<string,List<string>> relations_list;
 
        private int row { get; set; } // selected row in dgv
        private int col { get; set; } // selected collumn in dgv

        private bool on_edit { get; set; } // true ? edit in text_box (4 select reference cells)

        private HistoryBuffer<KeyValuePair<String, String>> history;

        public MainWindow()
        {
 
            InitializeComponent();
            GenerateEmptySpreadSheet();
            AddHotKeys();
            history = new HistoryBuffer<KeyValuePair<String, String>>();
        }

        #region SpreadSheet generator
        /* create start struct of spreadsheet */
        protected void GenerateEmptySpreadSheet()
        {
            on_edit = false; // by default
            relations_list = new Dictionary<string, List<string>>();
            /* will hold actual data after calculations */
            DataTable view_table = new DataTable("View");
            /* will hold backend expressions like =12+n*/
            DataTable back_table = new DataTable("Back");

            //faster ther i='a'; i<'z'; i++
            string alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            char[] alphas = alphabet.ToCharArray();
            for (int i = 0; i < alphas.Length; i++)
            {
                String str = alphas[i].ToString();
                view_table.Columns.Add(str);
                view_table.Rows.Add("");
                // faster ther .copy
                back_table.Columns.Add(str);
                back_table.Rows.Add("");
            }

            generateTable(back_table, view_table, alphas);


            // Create a DataSet and put both tables in it.
            SpreadSheet = new DataSet("SpreadSheet");
            SpreadSheet.Tables.Add(view_table);
            SpreadSheet.Tables.Add(back_table);
            spread_sheet_view.ItemsSource = SpreadSheet.Tables["View"].DefaultView;


        }
        private async void generateTable(DataTable back_table, DataTable view_table, char[] alphas)
        {

            int max = alphas.Length;
            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < max; j++)
                {
                    String str = alphas[i].ToString() + alphas[j].ToString();
                    view_table.Columns.Add(str);
                    back_table.Columns.Add(str);
                }
            }
            max = alphas.Length*3;
            for (int i = 0; i < max; i++)
            {
                view_table.Rows.Add("");
                back_table.Rows.Add("");
            }

        }
        //Recreate relations list to update calc dynamicly
        protected async void createRelations()
        {
            
            String error;
            for (int i = 0; i < SpreadSheet.Tables["Back"].Rows.Count; i++)
            {
                for (int j = 0; j < SpreadSheet.Tables["Back"].Columns.Count; j++)
                {
                    String input = SpreadSheet.Tables["Back"].Rows[i][j].ToString();
                    if (input.Length!=0)
                    {
                        string key = j.ToString() + ":" + i.ToString();
                        relations_list.Add(key, GridViewHelper.getReferences(SpreadSheet, input, out error));
                    } 
                }
            }
        }

        /* Set title for each row like 1,2...n*/
        private void Grid1_LoadingRow(object sender, DataGridRowEventArgs e)
        {
            var id = e.Row.GetIndex();
            id++;
            e.Row.Header = id.ToString();
        }
        #endregion SpreadSheet generator

        #region Menu
        /* On click item close program */
        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            Environment.Exit(1);
        }
        private void MenuItem_Open_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog();
            if (openFileDialog.ShowDialog() == true)
            {
                try
                {
                    
                    SpreadSheet = new DataSet("SpreadSheet"); ;
                    SpreadSheet.ReadXml(openFileDialog.FileName, XmlReadMode.ReadSchema);
                    spread_sheet_view.ItemsSource = SpreadSheet.Tables["View"].DefaultView;
                    createRelations();
                }
                catch (Exception)
                {
                    MessageBox.Show("Cannot read file");
                    throw;
                }
                
            }
                
        }
        private void MenuItem_Save_Click(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document"; // Default file name
            dlg.DefaultExt = ".tst"; // Default file extension
            dlg.Filter = "Text documents (.tst)|*.tst"; // Filter files by extension

            // Show save file dialog box
            Nullable<bool> result = dlg.ShowDialog();

            // Process save file dialog box results
            if (result == true)
            {
                // Save document
                string filename = dlg.FileName;
                try
                {
                    SpreadSheet.WriteXml(filename, XmlWriteMode.WriteSchema);
                }
                catch (Exception)
                {
                    MessageBox.Show("Cannot write file");
                    throw;
                }
                
            }
            
        }

        #endregion Menu

        #region HotKeys
        private void AddHotKeys()
        {
            try
            {
                RoutedCommand firstSettings = new RoutedCommand();
                firstSettings.InputGestures.Add(new KeyGesture(Key.O, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(firstSettings, MenuItem_Open_Click));

                RoutedCommand secondSettings = new RoutedCommand();
                secondSettings.InputGestures.Add(new KeyGesture(Key.S, ModifierKeys.Control));
                CommandBindings.Add(new CommandBinding(secondSettings, MenuItem_Save_Click));
            }
            catch (Exception err)
            {
                throw new ArgumentException(string.Format("{0} is error", err));

                //handle exception error
            }
        }
        #endregion HotKeys

        #region DataGrid
        private void spread_sheet_view_CellEditEnding(object sender, DataGridCellEditEndingEventArgs e)
        {
            var tmp = e.EditingElement as TextBox;
            Errors.errors error;
            if(!on_edit)
                editingDone(tmp.Text,row,col,out error);
        }

        private void spread_sheet_view_GotFocus(object sender, RoutedEventArgs e)
        {
            if (spread_sheet_view.CurrentCell.Column != null)
            { 
                if (on_edit)// if user write expression add ref (like AA12) on click
                {
                    int curr_row = spread_sheet_view.Items.IndexOf(spread_sheet_view.CurrentItem) + 1;
                    String column_letter = spread_sheet_view.CurrentCell.Column.Header.ToString();
                    real_data_text_box.Text += column_letter + curr_row.ToString();
                }
                else
                {
                    row = spread_sheet_view.Items.IndexOf(spread_sheet_view.CurrentItem);
                    col = spread_sheet_view.CurrentCell.Column.DisplayIndex;

                    String tmp_cell_data = SpreadSheet.Tables["Back"].Rows[row][col].ToString();
                    history.addHistoryItem(new KeyValuePair<String, String>(col.ToString()+":"+row.ToString(), tmp_cell_data));
                    real_data_text_box.Text = tmp_cell_data;
                    on_edit = false;
                }
            }
        }
        private void spread_sheet_view_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Delete) clearCell();

            if (!on_edit) return;

            if (e.Key != Key.Enter) return;

            e.Handled = true;
            on_edit = false;
            real_data_text_box.Focus();

        }
        #endregion DataGrid


        #region TextEditor
        private void real_data_text_box_KeyUp(object sender, KeyEventArgs e)
        {
            Errors.errors error;

            if (e.Key == Key.Escape)
            {
                e.Handled = true;
                KeyValuePair<String, String> prev_val;
                history.undo(out prev_val);
                real_data_text_box.Text = prev_val.Value;
                if (!editingDone(prev_val.Value, row, col, out error)) return;
                on_edit = false;
                spread_sheet_view.Focus();
            }

            if (e.Key != Key.Enter) return;

            e.Handled = true;
            var tmp = sender as TextBox;
            if(!editingDone(tmp.Text,row,col,out error)) return;

            on_edit = false;
            spread_sheet_view.Focus();

            GridViewHelper.SelectCellByIndex(spread_sheet_view, row, col);

        }

        private void real_data_text_box_TextChanged(object sender, TextChangedEventArgs e)
        {
            String tmp = real_data_text_box.Text;
            if (tmp.IndexOf('=') >= 0)
                on_edit = true;
        }
        #endregion TextEditor

        #region SupportFunctions
        protected bool editingDone(String input,int row, int col, out Errors.errors error, bool update = false)
        {

            if (input == null)
                input = "";
            bool isText = false;
            bool isNumeric = GridViewHelper.isNumeric(input);
            string key = col.ToString() + ":" + row.ToString();
            string prev_val = SpreadSheet.Tables["View"].Rows[row][col].ToString();

            //List of posible errors can be watch in class Errors
            error = Errors.errors.no_error;
            String checked_string = input;
            

            if (!isNumeric)
                isText = GridViewHelper.isString(input, out checked_string);

            if (isNumeric || isText)
            {
                SpreadSheet.Tables["Back"].Rows[row][col] = input;
                SpreadSheet.Tables["View"].Rows[row][col] = checked_string;
                if (!update)
                {
                    searchUpdates(key, update);
                    history.addHistoryItem(new KeyValuePair<String, String>(key, input));

                }

                return true;
            }

            // if string is expression 
            else if ((input.Length>0) && (input[0] == '='))
            {
                Stack<string> pn_struct;

                //Decode string to cross language
                CalcEngine.Decoder.Decode(ref input);
                //Convert string by polish notation in struct
                CalcEngine.Decoder.Converter(input, out pn_struct);
                List<string> refer_list;
                string[,] pointer_list = GridViewHelper.getReferences(SpreadSheet, input, out error, out refer_list);

                
                if ((refer_list!=null)&&(!update))
                {
                    relations_list.Remove(key);
                    relations_list.Add(key, refer_list);
                    isSelfLoop(key, refer_list, out error);
                    isInfLop(key, refer_list, out error);
                }
                   
                if (error == Errors.errors.no_error)
                {
                    string result = CalcEngine.Calculator.Calculate(pn_struct, pointer_list,out error).ToString();
                    if (error == Errors.errors.no_error)
                    {
                        SpreadSheet.Tables["Back"].Rows[row][col] = input;
                        SpreadSheet.Tables["View"].Rows[row][col] = result;

                        searchUpdates(key, update);
                        if(error!= Errors.errors.no_error)
                        {
                            if (!update)
                            { 
                                SpreadSheet.Tables["View"].Rows[row][col] = prev_val;
                                searchUpdates(key, update);
                                SpreadSheet.Tables["View"].Rows[row][col] = Errors.getErrorDesc(error);
                            }
                            
                            return false;
                        }
                        history.addHistoryItem(new KeyValuePair<String, String>(key, input));
                        return true;
                    }

                }

                SpreadSheet.Tables["View"].Rows[row][col]=error;
            }
            else
                SpreadSheet.Tables["View"].Rows[row][col]="#not allowed";

            return false;
            
        }

        protected bool clearCell()
        {
            SpreadSheet.Tables["Back"].Rows[row][col] = "";
            SpreadSheet.Tables["View"].Rows[row][col] = "";
            real_data_text_box.Text = "";
            return true;
        }

        protected async void searchUpdates(String curr_cell, bool update = false)
        {
            Errors.errors error = Errors.errors.no_error;
            if (relations_list.Count == 0)
                return;
            List<string> child_refer = (relations_list.ContainsKey(curr_cell))?relations_list[curr_cell]:null;
            foreach (KeyValuePair<string, List<string>> item in relations_list)
            {
                if (item.Value.IndexOf(curr_cell) >= 0)
                {
                    if ((child_refer!=null) &&(child_refer.IndexOf(item.Key) >= 0))
                    {
                        if(!update)
                            relations_list.Remove(curr_cell);
                        error = Errors.errors.infloop;
                        return;
                    }

                    string[] parts = item.Key.Split(':');
                    int col = int.Parse(parts[0]);
                    int row = int.Parse(parts[1]);
                    String input = SpreadSheet.Tables["Back"].Rows[row][col].ToString();
                    editingDone(input, row, col,out error, true);
                    if (error != null) return;
                }
            }
        }

        #endregion SupportFunctions

        #region LoopsCheckers
        protected void isSelfLoop(string self_key, List<string> refer_list, out Errors.errors error)
        {
            error = Errors.errors.no_error;
            if (refer_list.IndexOf(self_key) >= 0) { error = Errors.errors.selfloop; };
        }
        protected void isInfLop(string curr_cell, List<string> refer_list, out Errors.errors error)
        {
            error = Errors.errors.no_error;
            if (relations_list.Count == 0) return;

            List<string> child_refer = (relations_list.ContainsKey(curr_cell)) ? relations_list[curr_cell] : null;
            foreach (KeyValuePair<string, List<string>> item in relations_list)
            {
                if (item.Value.IndexOf(curr_cell) >= 0)
                {
                    if ((child_refer != null) && (child_refer.IndexOf(item.Key) >= 0))
                    {
                        error = Errors.errors.infloop;
                        return;
                    }
                }
            }
        }
        #endregion LoopsCheckers

    }
}
