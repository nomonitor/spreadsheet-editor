﻿using System;
using System.Data;
using System.Windows;
using System.Windows.Media;
using System.Windows.Controls;
using System.Collections.Generic;
using System.Windows.Controls.Primitives;
using System.Text.RegularExpressions;

namespace SpreadSheetEditor
{
    class GridViewHelper
    {
        public static void SelectCellByIndex(DataGrid dataGrid, int rowIndex, int columnIndex)
        {
            if (!dataGrid.SelectionUnit.Equals(DataGridSelectionUnit.Cell))
                throw new ArgumentException("The SelectionUnit of the DataGrid must be set to Cell.");

            if (rowIndex < 0 || rowIndex > (dataGrid.Items.Count - 1))
                throw new ArgumentException(string.Format("{0} is an invalid row index.", rowIndex));

            if (columnIndex < 0 || columnIndex > (dataGrid.Columns.Count - 1))
                throw new ArgumentException(string.Format("{0} is an invalid column index.", columnIndex));

            dataGrid.SelectedCells.Clear();

            object item = dataGrid.Items[rowIndex]; //=Product X
            DataGridRow row = dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow;
            if (row == null)
            {
                dataGrid.ScrollIntoView(item);
                row = dataGrid.ItemContainerGenerator.ContainerFromIndex(rowIndex) as DataGridRow;
            }
            if (row != null)
            {
                DataGridCell cell = GetCell(dataGrid, row, columnIndex);
                if (cell != null)
                {
                    DataGridCellInfo dataGridCellInfo = new DataGridCellInfo(cell);
                    dataGrid.SelectedCells.Add(dataGridCellInfo);
                    cell.Focus();
                }
            }
        }
        public static DataGridCell GetCell(DataGrid dataGrid, DataGridRow rowContainer, int column)
        {
            if (rowContainer != null)
            {
                DataGridCellsPresenter presenter = FindVisualChild<DataGridCellsPresenter>(rowContainer);
                if (presenter == null)
                {
                    /* if the row has been virtualized away, call its ApplyTemplate() method
                     * to build its visual tree in order for the DataGridCellsPresenter
                     * and the DataGridCells to be created */
                    rowContainer.ApplyTemplate();
                    presenter = FindVisualChild<DataGridCellsPresenter>(rowContainer);
                }
                if (presenter != null)
                {
                    DataGridCell cell = presenter.ItemContainerGenerator.ContainerFromIndex(column) as DataGridCell;
                    if (cell == null)
                    {
                        /* bring the column into view
                         * in case it has been virtualized away */
                        dataGrid.ScrollIntoView(rowContainer, dataGrid.Columns[column]);
                        cell = presenter.ItemContainerGenerator.ContainerFromIndex(column) as DataGridCell;
                    }
                    return cell;
                }
            }
            return null;
        }
        public static T FindVisualChild<T>(DependencyObject obj) where T : DependencyObject
        {
            for (int i = 0; i < VisualTreeHelper.GetChildrenCount(obj); i++)
            {
                DependencyObject child = VisualTreeHelper.GetChild(obj, i);
                if (child != null && child is T)
                    return (T)child;
                else
                {
                    T childOfChild = FindVisualChild<T>(child);
                    if (childOfChild != null)
                        return childOfChild;
                }
            }
            return null;
        }

        public static bool isString(String input, out String checked_string)
        {
            checked_string = input;
            if (input.Length > 1)
                if (input[0] == '\'')
                {
                    checked_string = input.Substring(1);
                    return true; 
                }
            return false;
        }
        public static bool isNumeric(object Expression)
        {
            double retNum;

            bool isNum = Double.TryParse(Convert.ToString(Expression), System.Globalization.NumberStyles.Any, System.Globalization.NumberFormatInfo.InvariantInfo, out retNum);
            return isNum;
        }
        
        /* creating pair <key, value> to replace refecence to calc val*/
        public static string[,] getReferences(DataSet ds, string input, out Errors.errors error, out List<string> refer_list)
        {
            error = Errors.errors.no_error;
            refer_list = new List<string>();
            var tmp_list = new List<KeyValuePair<string, string>>();

            //Example AA12 or A12345
            MatchCollection matches = Regex.Matches(input, @"[A-Z]{1,2}[0-9]{1,5}");

            foreach (Match match in matches)
            {
                foreach (Capture capture in match.Captures)
                {
                    //Get parts of key such AA (col) and 12 (row)
                    Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
                    Match result = re.Match(capture.Value);

                    string col = result.Groups[1].Value.ToString().ToUpper();
                    int row = int.Parse(result.Groups[2].Value);
                    row--;

                    refer_list.Add(ds.Tables["View"].Columns[col].Ordinal.ToString() + ":" + row.ToString());

                    String cell_data = ds.Tables["View"].Rows[row][col].ToString();
                    if (cell_data.Length != 0)
                        tmp_list.Add(new KeyValuePair<string, string>(capture.Value, cell_data));
                    else
                    {
                        // can't find calc value, return short error descr
                        refer_list = null;
                        error = Errors.errors.no_reference;
                        return null;
                    }
                        
                }
            }

            if (tmp_list.Count == 0)
            {
                refer_list = null;
                return null;
            }

            string[,] referernces = new string[2, tmp_list.Count];
            for (int i = 0; i < tmp_list.Count; i++)
            {
                referernces[0, i] = tmp_list[i].Key;
                referernces[1, i] = tmp_list[i].Value;
            }
            return referernces;
        }
        public static List<string> getReferences(DataSet ds, string input, out string error)
        {
            error = null;
            List<string> refer_list = new List<string>();

            //Example AA12 or A12345
            MatchCollection matches = Regex.Matches(input, @"[A-Z]{1,2}[0-9]{1,5}");

            foreach (Match match in matches)
            {
                foreach (Capture capture in match.Captures)
                {
                    //Get parts of key such AA (col) and 12 (row)
                    Regex re = new Regex(@"([a-zA-Z]+)(\d+)");
                    Match result = re.Match(capture.Value);

                    string col = result.Groups[1].Value.ToString().ToUpper();
                    int row = int.Parse(result.Groups[2].Value);
                    row--;

                    refer_list.Add(ds.Tables["View"].Columns[col].Ordinal.ToString() + ":" + row.ToString());

                }
            }
            return refer_list;
        }
    }
}
