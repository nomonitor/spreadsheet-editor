﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpreadSheetEditor
{
    //Must be connected with dg and btns later
    class HistoryBuffer<T>
    {
        const int max_items = 150;
        //KeyValuePair<string, string>
        private CustomStack<T> buffer_pre = new CustomStack<T>();
        private CustomStack<T> buffer_post = new CustomStack<T>();


        public bool redo(out T output)
        {
            output = default(T);
            if (buffer_pre.Count() == 0)
                return false;

            output = buffer_pre.Pop();
            buffer_post.Push(output);

            return true;
        }

        public bool undo(out T output)
        {
            output = default(T);
            if (buffer_post.Count() == 0)
                return false;

            output = buffer_post.Pop();
            buffer_pre.Push(output);
            return true;
        }

        public void addHistoryItem(T input)
        {
            buffer_post.Clear();
            if (buffer_post.Count() >= max_items)
                buffer_pre.Remove(0);
            buffer_pre.Push(input);
        }
    }
    #region CustomStack
    public class CustomStack<T>
    {
        private List<T> items = new List<T>();

        public void Push(T item)
        {
            items.Add(item);
        }
        public T Pop()
        {
            if (items.Count > 0)
            {
                T temp = items[items.Count - 1];
                items.RemoveAt(items.Count - 1);
                return temp;
            }
            else
                return default(T);
        }
        public int Count()
        {
           return items.Count;
        }
        public void Clear()
        {
            items.Clear();
        }
        public void Remove(int itemAtPosition)
        {
            items.RemoveAt(itemAtPosition);
        }
    }
    #endregion CustomStack
}
