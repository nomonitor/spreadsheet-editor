﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;

namespace SpreadSheetEditor.CalcEngine
{
    public class Decoder//coment public
    {
        //public Stack<string> BufferStack
        //{
        //    get { return OperationLine; }
        //}
        //Dictionary equivalents
        //| + | - | * | / | ^ | sqrt | cos | sin | arccos| arcsin | tg | ctg | arctg | arcctg | lg | log | Pi |
        //| + | - | * | / | ^ |  &   |  @  |  #  |   $   |   %    |  | |  ?  |  >    |    <   | £  |  ¥  | µ  |
        internal static string[,] Dictionary = {{"(",")","+","-","*","/","^","sqrt","cos","sin","arccos","arcsin","tg","ctg","arctg","arcctg","lg","Pi","!"},//Name
                                        {"(",")","+","-","*","/","^","&","@","#","$","%","|","?",">","<","£","µ","!"},//Symbol
                                        {"0","1","2","2","3","3","4","4","4","4","4","4","4","4","4","4","4","4","4"}};//Priority

        //Decode string to cross language
        public static void Decode(ref string str)
        {

            str = str.Replace(")", "*1)");
            for (int i = 2; i < Dictionary.Length / 3; i++)
            {
                str = str.Replace(Dictionary[0, i], Dictionary[1, i]);
            }
        }
        //Convert string by polish notation in struct
        public static void Converter(string str, out Stack<string> OperationLine)
        {
            str = str.Replace('.', ',');
            Stack<StackStruct> OperationStack = new Stack<StackStruct>();
            OperationLine = new Stack<string>();
            string temp = "";
            for (int i = 0; i < str.Length; i++)
            {

                int j = 0;
                if (Regex.IsMatch(str[i].ToString(), "[0-9,A-Z,a-z,.]"))
                {
                        temp += str[i];
                }
                else if (RegMatch(str[i].ToString(), out j))
                {
                    if (temp != "")
                    { OperationLine.Push(temp); temp = ""; }
                    StackOperations(j, OperationStack, OperationLine);
                }
            }
            if (temp != "")
                OperationLine.Push(temp);
            while (OperationStack.Count > 0)
            { OperationLine.Push(OperationStack.Pop().OperationSymbol.ToString()); }
            OperationLine = ReverseStack(OperationLine);
        }
        private static Stack<string> ReverseStack(Stack<string> temp)
        {
            Stack<string> AnotherOne = new Stack<string>();
            foreach (var n in temp)
            {
                AnotherOne.Push(n);
            }
            return AnotherOne;
        }
        private static void StackOperations(int i, Stack<StackStruct> OperationStack, Stack<String> OperationLine)
        {
            if (OperationStack.Count != 0)
            {
                if (Dictionary[1, i] == ")")
                {
                    do
                    {
                        OperationLine.Push(OperationStack.Pop().OperationSymbol.ToString());
                    } while (OperationStack.Peek().OperationSymbol != '(');
                    OperationStack.Pop();
                }
                else if ((OperationStack.Peek().OperationPriority < int.Parse(Dictionary[2, i])) || (Dictionary[1, i] == "("))
                {
                    if (((Dictionary[1, i] == "µ") & OperationLine.Count > 0))
                    {
                        if (IsNumber(OperationLine.Peek()))
                            OperationStack.Push(new StackStruct(Char.Parse(Dictionary[1, 4]), int.Parse(Dictionary[2, 4])));
                    }

                    OperationStack.Push(new StackStruct(Char.Parse(Dictionary[1, i]), int.Parse(Dictionary[2, i])));
                }
                else
                {
                    while ((OperationStack.Count != 0) && (OperationStack.Peek().OperationPriority >= int.Parse(Dictionary[2, i])))
                    {
                        OperationLine.Push(OperationStack.Pop().OperationSymbol.ToString());
                    }
                    OperationStack.Push(new StackStruct(Char.Parse(Dictionary[1, i]), int.Parse(Dictionary[2, i])));
                }
            }
            else
            {
                if (((Dictionary[1, i] == "µ") & OperationLine.Count > 0))
                {
                    if (IsNumber(OperationLine.Peek()))
                        OperationStack.Push(new StackStruct(Char.Parse(Dictionary[1, 4]), int.Parse(Dictionary[2, 4])));
                }

                OperationStack.Push(new StackStruct(Char.Parse(Dictionary[1, i]), int.Parse(Dictionary[2, i])));
            }
        }

        //If match return weight
        private static bool RegMatch(string str, out int j)
        {
            for (int i = 0; i < Dictionary.Length / 3; i++)
            {
                if (str == Dictionary[1, i])
                { j = i; return true; }
            }
            j = 0;
            return false;
        }
        private static bool IsNumber(string str)
        {
            double Num;
            bool isNum = double.TryParse(str, out Num);

            if (isNum)

                return true;

            else

                return false;
        }
        public struct StackStruct
        {
            public char OperationSymbol;
            public int OperationPriority;
            public StackStruct(char a, int b)
            {
                OperationSymbol = a;
                OperationPriority = b;
            }
        }
    }
}
