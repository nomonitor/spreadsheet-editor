﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace SpreadSheetEditor.CalcEngine
{
    public class Calculator//set private
    {
        public static double Calculate(Stack<string> OperationLine, string[,] Letters, out Errors.errors error)
        {
            error = Errors.errors.wrong_expressions;
            Stack<string> TempStack = new Stack<string>(); string k;
            foreach (var item in OperationLine)
            {
                try
                {
                    if (CheckType(item, 2, 6))
                        TempStack.Push(Operations(double.Parse(TempStack.Pop()), double.Parse(TempStack.Pop().ToString()), item).ToString());
                    else if (item == "µ")
                        TempStack.Push(Math.PI.ToString());
                    else if (CheckType(item, 7, 18))
                        TempStack.Push(Operations(double.Parse(TempStack.Pop()), item).ToString());
                    else if (CheckIsLetter(item, out k, Letters))
                    { TempStack.Push(k); }
                    else
                        TempStack.Push(item);
                }
                catch (Exception)
                {
                    return 0;
                }
                    
            }
            if (TempStack.Count != 0)
            {
                error = Errors.errors.no_error;
                return double.Parse(TempStack.Pop());
            }
            
            return 0;
        }

        //If it's letter (symbol),  get command and execute request
        private static bool CheckIsLetter(string temp, out string k, string[,] L)
        {
            int n; k = null;
            double m;
            if (int.TryParse(temp, out n))
                return false;
            if (double.TryParse(temp, out m))
                return false;
            for (int i = 0; i < L.Length / 2; i++)
            {
                if (temp == L[0, i])
                {
                    k = L[1, i];
                    return true;
                }
            }
            return false;
        }
        private static bool CheckType(string Item, int StartIndex, int FinishIndex)
        {
            for (int i = StartIndex; i <= FinishIndex; i++)
            {
                if (Item == Decoder.Dictionary[1, i])
                    return true;
            }
            return false;
        }
        private static double Operations(double b, double a, string Operation)
        {
            switch (Operation)
            {
                case "+"://addition
                    return a + b;
                case "-"://subtraction
                    return a - b;
                case "*"://multiply
                    return a * b;
                case "/"://division
                    return a / b;
                case "^"://pow
                    return Math.Pow(a, b);
                case "&"://sqrt
                    return Math.Pow(a, 1 / b);
                default:
                    return 0.0;
            }
        }
        private static double Operations(double a, string Operation)
        {
            switch (Operation)
            {
                case "@"://cos
                    return Math.Cos(a);
                case "#"://sin
                    return Math.Sin(a);
                case "$"://arccos
                    return Math.Acos(a);
                case "%"://arcsin
                    return Math.Asin(a);
                case "|"://tg
                    return Math.Tan(a);
                case "?"://ctg
                    return Math.Cos(a) / Math.Sin(a);
                case ">"://arctg
                    return Math.Atan(a);
                case "<"://arcctg
                    if (a >= 0)
                        return Math.Asin(1 / Math.Sqrt(1 + a * a));
                    else
                        return Math.PI - Math.Asin(1 / Math.Sqrt(1 + a * a));
                case "£"://lg
                    return Math.Log10(a);
                case "¥"://log
                    return Math.Log(a);
                case "!"://Factorial
                    return Factorial(a);
                default:
                    return 0.0;
            }
        }

        private static double Factorial(double a)
        {
            double temp = 1;
            for (int i = 1; i < a; i++)
            {
                temp *= i;
            }
            return temp;
        }
    }
}
